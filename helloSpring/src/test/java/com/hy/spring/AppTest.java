package com.hy.spring;

import static org.junit.Assert.assertTrue;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.hy.entity.Book;
import com.hy.entity.Person;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

/**
 * Unit test for simple App.
 */
public class AppTest {

    ApplicationContext context = new ClassPathXmlApplicationContext("application2.xml");
    /**
     *
     * Rigorous Test :-)
     */
    @Test
    public void test1() {
        /**
         *ApplicationContext:接口  ioc容器
         *对象是在容器创建完成之前就被创建了
         * context.getBean("id"):通过id到容器里把这个对象取出来
         * 容器默认创建出来的对象是单实例的  多次get出来的对象是同一个对象
         * 创建对象是通过反射调用无参构造方法，必须要有
         * 通过peoperty可以注入属性，是通过类的set方法注入的，setter/getter都自动生成，不要随便写
         */

//        System.out.println("getBean之前");

//        NoSuchBeanDefinitionException: No bean named 'book' available

        Object book = context.getBean("book");

//  No qualifying bean of type 'com.hy.entity.Book' available:
// expected single matching bean but found 2: book,book2
        Book bean = context.getBean("book",Book.class);
//        Book book2 = (Book) context.getBean("book");
//        System.out.println(book==book2);
//        System.out.println("getBean之后");
        System.out.println(book);
    }

    @Test
    public void test2(){

//        Person person2 = (Person) context.getBean("person2");
        Person person4 = (Person) context.getBean("person4");
//        Object innerBook = context.getBean("innerBook");
        System.out.println(person4);


    }

    @Test
    public void test3() throws SQLException {

//        Person person2 = (Person) context.getBean("person2");
//        Person person5 = (Person) context.getBean("person5");
//        Object innerBook = context.getBean("innerBook");
        DruidDataSource dataSource = (DruidDataSource) context.getBean("dataSource");
        DruidPooledConnection connection = dataSource.getConnection();
        System.out.println(dataSource);
        System.out.println(connection);


    }
}
