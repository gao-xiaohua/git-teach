package com.hy.spring;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {


        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application2.xml");
        Object book = context.getBean("bookk");
        System.out.println(book);


    }
}
