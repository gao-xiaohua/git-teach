package com.hy.spring;

import static org.junit.Assert.assertTrue;

//import com.hy.Rent;
import com.hy.impl.RentImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        //No bean named 'rentImpl' available
        //No qualifying bean of type 'com.hy.impl.RentImpl' available
        RentImpl rent = (RentImpl) context.getBean("rentImpl");
        //错误的 getBean获取到的是一个动态代理类
//        RentImpl obj1 = (RentImpl) context.getBean("rentImpl");

        System.out.println(rent.getClass());
        rent.rentPerson(1, 1);

//        System.out.println("返回结果是="+i);
//        System.out.println("==============");
    }
}
